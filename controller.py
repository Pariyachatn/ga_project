import common
import random


def generate_generation():

    # clear bucket
    common.state["gen_bucket"].clear()

    for pop in common.state["pp_gen_bucket"]:

        province_temp = list(common.data["province"]).copy()

        # print(province_temp)

        chromosomes = {
            "map": [],
            "obj_value": 0.0,
            "fitness_value": 0.0,
            "probability": 0.0,
            "wor": 0.0
        }

        # initial random value of each city
        for (inx, data) in enumerate(province_temp):

            map_data = {
                "city": data,
                "value": pop[inx]
            }

            chromosomes["map"] += [map_data]

        chromosomes["map"] = sorted(
            chromosomes["map"], key=lambda x: x["value"])

        for (inx, chromo) in enumerate(chromosomes["map"]):
            chromo["value"] = pop[inx]

        # calculate object value
        for (inx, data) in enumerate(chromosomes["map"]):
            if inx > 0:
                before_inx = list(common.data["province"].keys()).index(
                    chromosomes["map"][inx-1]["city"])+1
                curr_inx = list(common.data["province"].keys()).index(
                    data["city"])+1
                distance = common.data["map"][curr_inx][before_inx]
                chromosomes["obj_value"] += distance

        chromosomes["fitness_value"] = round(
            1/chromosomes["obj_value"]*10000, 3)
        # print(chromosomes)

        common.state["gen_bucket"].append(chromosomes)

    sum_fitness_value = round(
        sum([x["fitness_value"] for x in common.state["gen_bucket"]]), 3)

    # print(sum_fitness_value)

    for i in range(len(common.state["gen_bucket"])):
        data = common.state["gen_bucket"][i]

        data["probability"] = round(data["fitness_value"]/sum_fitness_value, 3)

        if i > 0:
            before_data = common.state["gen_bucket"][i-1]
            data["wor"] = round(before_data["wor"]+data["probability"], 3)
        else:
            data["wor"] = data["probability"]

    print_gen_bucket()
    global_best_update()


def init_population():

    for i in range(common.config["population_size"]):
        common.state["pp_gen_bucket"].append([random.randrange(
            common.config["chr_rep_min"], common.config["chr_rep_max"]) for x in range(len(common.data["province"]))])

    generate_generation()
    # print_pp_gen_bucket()


def print_gen_bucket():
    for (inx, pop) in enumerate(common.state["gen_bucket"]):
        print("pop "+str(inx)+" :", pop)


def print_pp_gen_bucket():
    for (inx, data) in enumerate(common.state["pp_gen_bucket"]):
        print("pp_gen_bucket "+str(inx)+" :", data)


def global_best_update():
    max_obj = sorted(common.state["gen_bucket"],
                     key=lambda x: x["fitness_value"], reverse=True)[0]
    # print(max_obj)
    if common.state["global_best"] == {}:
        common.state["global_best"] = max_obj
    elif max_obj["fitness_value"] > common.state["global_best"]["fitness_value"]:
        common.state["global_best"] = max_obj
    # print(common.state["global_best"])


def cross_over():
    common.state["pp_gen_bucket"].clear()
    while len(common.state["pp_gen_bucket"]) < common.config["population_size"]:

        isDuplicate = True

        rand_3 = round(random.uniform(0, 1.001), 3)

        chromosomes_1 = {}
        chromosomes_2 = {}
        chromosomes_3 = {}

        while isDuplicate:
            rand_1 = round(random.uniform(0, 1.000), 3)
            print(rand_1)
            rand_2 = round(random.uniform(0, 1.000), 3)
            print(rand_2)

            # chromosomes_1
            chromosomes_1_filter = [x for x in filter(
                lambda x: x["wor"] >= rand_1, common.state["gen_bucket"])]
            chromosomes_1_sort = []

            if len([x for x in chromosomes_1_filter]) == 0:
                chromosomes_1_sort = sorted(
                    common.state["gen_bucket"], key=lambda x: x["wor"], reverse=True)
            else:
                chromosomes_1_sort = sorted(
                    chromosomes_1_filter, key=lambda x: x["wor"])

            chromosomes_1 = chromosomes_1_sort[0]

            # chromosomes_2
            chromosomes_2_filter = [x for x in filter(
                lambda x: x["wor"] >= rand_2, common.state["gen_bucket"])]
            chromosomes_2_sort = []
            if len(list(chromosomes_2_filter)) == 0:
                chromosomes_2_sort = sorted(
                    common.state["gen_bucket"], key=lambda x: x["wor"], reverse=True)
            else:
                chromosomes_2_sort = sorted(
                    chromosomes_2_filter, key=lambda x: x["wor"])
            chromosomes_2 = chromosomes_2_sort[0]

            isDuplicate = chromosomes_1["wor"] == chromosomes_2["wor"]

        # chromosomes_3
        chromosomes_3_filter = [x for x in filter(
            lambda x: x["wor"] >= rand_3, common.state["gen_bucket"])]
        chromosomes_3_sort = []
        if len(list(chromosomes_3_filter)) == 0:
            chromosomes_3_sort = sorted(
                common.state["gen_bucket"], key=lambda x: x["wor"], reverse=True)
        else:
            chromosomes_3_sort = sorted(
                chromosomes_3_filter, key=lambda x: x["wor"])
        chromosomes_3 = chromosomes_3_sort[0]

        chromosomes_1_split = [
            [x["value"] for x in chromosomes_1["map"]][0:2],
            [x["value"] for x in chromosomes_1["map"]][2:4],
            [x["value"] for x in chromosomes_1["map"]][4:6],
            [x["value"] for x in chromosomes_1["map"]][6:8],
            [x["value"] for x in chromosomes_1["map"]][8:10],
            [x["value"] for x in chromosomes_1["map"]][10:12],
            [x["value"] for x in chromosomes_1["map"]][12:15]
        ]

        chromosomes_2_split = [
            [x["value"] for x in chromosomes_2["map"]][0:2],
            [x["value"] for x in chromosomes_2["map"]][2:4],
            [x["value"] for x in chromosomes_2["map"]][4:6],
            [x["value"] for x in chromosomes_2["map"]][6:8],
            [x["value"] for x in chromosomes_2["map"]][8:10],
            [x["value"] for x in chromosomes_2["map"]][10:12],
            [x["value"] for x in chromosomes_2["map"]][12:15]
        ]

        chromosomes_3_split = [
            [x["value"] for x in chromosomes_3["map"]][0:2],
            [x["value"] for x in chromosomes_3["map"]][2:4],
            [x["value"] for x in chromosomes_3["map"]][4:6],
            [x["value"] for x in chromosomes_3["map"]][6:8],
            [x["value"] for x in chromosomes_3["map"]][8:10],
            [x["value"] for x in chromosomes_3["map"]][10:12],
            [x["value"] for x in chromosomes_3["map"]][12:15]
        ]

        # cross-over
        offspring_1 = chromosomes_1_split[6]+chromosomes_2_split[5]+chromosomes_1_split[4]+chromosomes_2_split[3]+chromosomes_1_split[2]+chromosomes_2_split[1]+chromosomes_1_split[0]
        offspring_2 = chromosomes_2_split[6]+chromosomes_1_split[5]+chromosomes_2_split[4]+chromosomes_1_split[3]+chromosomes_2_split[2]+chromosomes_1_split[1]+chromosomes_2_split[0]

        # mutation
        offspring_3 = chromosomes_3_split[2]+chromosomes_3_split[1]+chromosomes_3_split[4]+chromosomes_3_split[3]+chromosomes_3_split[6]+chromosomes_3_split[5]+chromosomes_3_split[0]

        print("chromosomes_1 :", chromosomes_1)
        print("chromosomes_2 :", chromosomes_2)

        print("offspring_1 :", offspring_1)
        print("offspring_2 :", offspring_2)
        print("offspring_3 :", offspring_3)

        if len(common.state["pp_gen_bucket"]) < common.config["population_size"]:
            common.state["pp_gen_bucket"].append(offspring_1)

        if len(common.state["pp_gen_bucket"]) < common.config["population_size"]:
            common.state["pp_gen_bucket"].append(offspring_2)

        if len(common.state["pp_gen_bucket"]) < common.config["population_size"]:
            common.state["pp_gen_bucket"].append(offspring_3)

    generate_generation()

def print_map_name():
    for (inx,data) in enumerate(common.state["global_best"]["map"]):
        city = common.data["province"].get(data["city"])
        print(str(inx),city)
    
    print("obj_value :",common.state["global_best"]["obj_value"])

def start_ga():
    for inx in range(common.config["generation_size"]):
        if inx == 0:
            init_population()
        else:
            cross_over()
            # pass

        print()

    print("global_best :", common.state["global_best"])
    print_map_name()
